//import des modules et fichiers
import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/lib/util/colors'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify)

//liste des paramètres Vuetify
const opts = {
    icons: {
        iconfont: 'md' || 'fa'
    },
    theme: {
        themes: {
            light: {
                background: colors.blue.accent4,
                background2: colors.white,
            },
            dark: {
                bakground: colors.blue.ligthen5
            }
        }
    }
}

export default new Vuetify(opts)