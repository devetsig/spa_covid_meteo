//import des modules et vues nécessaires au routage 
import Vue from 'vue'
import Router from 'vue-router'
import Accueil from '@/views/Accueil'
import Meteo from '@/views/Meteo'
import Covid from '@/views/Covid'
import Contrib from '@/views/Contrib'
import Erreur from '@/views/Erreur';


Vue.use(Router)

//définition des URLs
export default new Router({
  routes: [
    {
      path: '/',
      name: 'Accueil',
      component: Accueil
    },
    {
      path: '/meteo',
      name: 'Meteo',
      component: Meteo
    },
    {
      path: '/covid',
      name: 'Covid',
      component: Covid
    },
    {
      path: '/contrib',
      name: 'Contrib',
      component: Contrib
    },
    {
      path: '/*',
      name: 'Erreur',
      component: Erreur
    },
  ]
})
