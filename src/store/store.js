//import des modules et jeux de données vectorielles
import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios'
import frDepartGeojson from '../data/france-departement.json'

Vue.use(Vuex, Axios)

//instanciation du Store
export const store = new Vuex.Store({
    strict: true,
    state: {
        //on divise le state en 2 : une partie dédiée aux données météo, l'autre au COVID-19
        meteo: {
                    chart: null,
                    ville: "",
                    loading: false,
                    map : null,
                    url: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                    attribution:'&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
                    zoom: 10,
                    center: [49.03898, 2.07501],
                    show_wmap: true,
                    marker: null,
                    posts: [],
                    dataApi: {
                        temperatures: [],
                        dates: [],
                        humidite: [],
                        vent: []
                    },
                    temps: [],
                    dataApiBis: null,
                    windTemps: null,
                    humTemps: null,
        },
        virus: {
                  loading: false,
                  frDepartGeojson : frDepartGeojson,//importer le geoJSON des départements fr
                  depCoord: null,
                  dataApi: null,
                  departements: [],
                  date:null,
                  depCible: '',
                  depDate : '____-__-__',
                  depDeces: null,
                  depGueris: null,
                  depHospi : null,
                  depRea : null,
                  depNewHospi : null,
                  depNewRea: null,
                  depData: null,
                  depTempData: [],
                  depTempDate: null,
                  depTempDeces: null,
                  depTempGueris: null,
                  depTempDataBis: null,
                  chartLineDeces: null,
                  chartLineGueris: null,
        }
    },
    mutations: {
      SET_DEPS: (state, res) => {
        state.virus.dataApi = res.allLiveFranceData.slice(0, 100)
        state.virus.departements = res.allLiveFranceData.slice(0, 100).map(allLiveFranceData => {
            return allLiveFranceData.nom
        })
        state.virus.date = res.allLiveFranceData[0].date
      },
      SET_DATA_BY_DEP: (state, res) => {
        state.virus.depDeces = res.LiveDataByDepartement[0].deces
        state.virus.depGueris = res.LiveDataByDepartement[0].gueris
        state.virus.depHospi = res.LiveDataByDepartement[0].hospitalises
        state.virus.depRea = res.LiveDataByDepartement[0].reanimation
        state.virus.depNewHospi = res.LiveDataByDepartement[0].nouvellesHospitalisations
        state.virus.depNewRea = res.LiveDataByDepartement[0].nouvellesReanimations
        state.virus.depDate = res.LiveDataByDepartement[0].date
      },
      SET_SELECTED_DEP: (state, res) => {
        state.virus.depTempData = res.allDataByDepartement.slice(Math.max(res.allDataByDepartement.length - 60, 0));
        state.virus.depTempDate = state.virus.depTempData.map(depTempData => {
            return depTempData.date
        });
        state.virus.depTempDeces = state.virus.depTempData.map(depTempData => {
          return depTempData.deces
        });
        state.virus.depTempGueris = state.virus.depTempData.map(depTempData => {
          return depTempData.gueris
        });
    
        var a = {};
        for(var i=0; i<state.virus.depTempDate.length; i++) {
              a[state.virus.depTempDate[i]] = state.virus.depTempDeces[i];
        }
        state.virus.chartLineDeces = a;

        var b = {};
        for(var i=0; i<state.virus.depTempDate.length; i++) {
              b[state.virus.depTempDate[i]] = state.virus.depTempGueris[i];
        }
        state.virus.chartLineGueris = b;

        //sélection du département à vectoriser
        var item = state.virus.frDepartGeojson.features.find(feature => feature.properties.dep_name === state.virus.depCible)
        state.virus.depCoord = item.geometry.coordinates;

        if(item.geometry.type === "Polygon") {
          state.virus.depCoord = item.geometry.coordinates[0];

          for (var i = 0; i < state.virus.depCoord.length; i++) {
                state.virus.depCoord[i].reverse()
              }
        }
        else {
          state.virus.depCoord = item.geometry.coordinates;
          for (var i = 0; i < state.virus.depCoord.length; i++) {
              state.virus.depCoord[i][0]
              for (var y = 0; y < state.virus.depCoord[i][0].length; y++) {
                  state.virus.depCoord[i][0][y].reverse()
                }
            }

        }
      },
    },
    actions: {
        getTemps() {

          this.state.meteo.loading = true;

          Axios
                .get("https://api.openweathermap.org/data/2.5/forecast", {
                    params: {
                      q: this.state.meteo.ville,
                      units: "metric",
                      appid: ""//entrez votre identifiant API
                    }
                  })
                .then(response => {
                    this.state.meteo.temps = response.data;
                    this.state.meteo.center = response.data.city.coord;
                    this.state.meteo.dataApi.dates = response.data.list.map(list => {
                        return list.dt_txt;
                      });
                    this.state.meteo.dataApi.temperatures = response.data.list.map(list => {
                        return list.main.temp;
                      });
                    this.state.meteo.dataApi.humidite = response.data.list.map(list => {
                        return list.main.humidity;
                      });
                    this.state.meteo.dataApi.vent = response.data.list.map(list => {
                        return list.wind.speed;
                      });
                                        
                    var z = {};
                    for(var i=0; i<this.state.meteo.dataApi.dates.length; i++) {
                        z[this.state.meteo.dataApi.dates[i]] = this.state.meteo.dataApi.temperatures[i];
                    }
                    this.state.meteo.dataApiBis = z

                    var x = {};
                    for(var i=0; i<this.state.meteo.dataApi.dates.length; i++) {
                        x[this.state.meteo.dataApi.dates[i]] = this.state.meteo.dataApi.humidite[i];
                    }
                    this.state.meteo.humTemps = x

                    var y = {};
                    for(var i=0; i<this.state.meteo.dataApi.dates.length; i++) {
                        y[this.state.meteo.dataApi.dates[i]] = this.state.meteo.dataApi.vent[i];
                    }
                    this.state.meteo.windTemps = y
                    
                })
                .catch(error => {
                    console.log(error);
                  })
        },
        getDep({commit}) {
          Axios.get('https://coronavirusapi-france.now.sh/AllLiveData')
                  .then(response => {
                      commit('SET_DEPS', response.data)
                  })
                  .catch(error => {
                      console.log(error);
                    })
        },
        getCovidData({commit}) {
          
          this.state.virus.loading = true;

          //Tri et filtre des données de l'API coronavirusapi-france
          Axios.get('https://coronavirusapi-france.now.sh/LiveDataByDepartement?Departement=' + this.state.virus.depCible)
                  .then(response => {
                    commit('SET_DATA_BY_DEP', response.data)
                })
                  .catch(error => {
                      console.log(error);
                    });
          
          //Données de l'API coronavirusapi-france par département sélectionné
          Axios.get('https://coronavirusapi-france.now.sh/AllDataByDepartement?Departement=' + this.state.virus.depCible)
                    .then(response => {
                      commit('SET_SELECTED_DEP', response.data)
                  })
                    .catch(error => {
                      console.log(error);
                    });
        },        
    }
})
