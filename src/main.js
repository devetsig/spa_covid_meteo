// import des modules
import Vue from 'vue'
import App from './App'
import router from './router'
import vuetify from './plugins/vuetify'
import {store} from './store/store'
import L from 'leaflet'
import 'leaflet/dist/leaflet.css'
import Chartkick from 'vue-chartkick'
import Chart from 'chart.js'
import ChartAnnotationsPlugin from 'chartjs-plugin-annotation'

//pr afficher les images des leaflet-markers
delete L.Icon.Default.prototype._getIconUrl  
L.Icon.Default.mergeOptions({  
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),  
  iconUrl: require('leaflet/dist/images/marker-icon.png'),  
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')  
})

//pour activer le plugin d'annotations de graphiques chartJS
Chart.plugins.register(ChartAnnotationsPlugin)
//pour utiliser Chartkick
Vue.use(Chartkick.use(Chart))

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  vuetify,
  store: store,
  router,
  components: { App },
  template: '<App/>'
})
