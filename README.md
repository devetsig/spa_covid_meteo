# Application monopage (SPA) covid19/météo

> Un projet VueJS, déployé sous Firebase, qui rassemble deux applications météo et COVID19 en France. Les données utilisées ont été extraites d'[OpenWeatherMapAPI](https://openweathermap.org/api) et de l'API [Datagouv](https://www.data.gouv.fr/fr/reuses/coronavirusapi-france/) dédié au partage des données relatives au COVID-19 du Ministère de la Santé. 
Pour accéder à la page web : [https://covid-france-app.web.app/#/](https://covid-france-app.web.app/#/)

![](src/assets/img/screen1.png)

![](src/assets/img/screen2.png)

## Fonctionnement

On récupère les données des deux APIs grâce au client HTTP Axios puis on les stocke et les gère dans un store VueX (bibliothèque de gestion d'états). Elles sont ensuite redistribuées dans une arborescence de composants Vuetify. Avec Vue-Router, ces derniers sont appelés par des Vues qui possèdent chacune des paramètres de route spécifiques. Enfin, les modules Vue2-Leaflet et Vue-Chloroplet accélèrent la création cartes-web interactives tandis que Vue-Chartkick met à disposition les différents graphiques.

# Librairies utilisées :
* [Vue.js](https://vuejs.org/)
* [VueX](https://vuex.vuejs.org/)
* [Axios](https://github.com/axios/axios)
* [Vue-Router](https://router.vuejs.org/)
* [Vuetify](https://vuetifyjs.com/en/)
* [Vue2Leaflet.js](https://vue2-leaflet.netlify.app/)
* [Vue-Chartkick](https://chartkick.com/vue)